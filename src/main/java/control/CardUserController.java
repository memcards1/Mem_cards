package control;

import connection.HibernateConnection;
import entity.CardUser;
import entity.CardUserId;
import org.hibernate.Session;
import view.ErrorPane;

public class CardUserController {

    public void save(CardUser cardUser) {
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(cardUser);
            session.getTransaction().commit();
        } catch (Exception ex) {
            new ErrorPane().displayError(ex.getLocalizedMessage(), "Exception");
        }
    }

    public void update(CardUser cardUser) {
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.update(cardUser);
            session.getTransaction().commit();
        } catch (Exception ex) {
            new ErrorPane().displayError(ex.getLocalizedMessage(), "Exception");
        }
    }

    public void delete(CardUser cardUser) {
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(cardUser);
            session.getTransaction().commit();
        } catch (Exception ex) {
            new ErrorPane().displayError(ex.getLocalizedMessage(), "Exception");
        }
    }

    public CardUser getById(CardUserId cardUserId) {
        CardUser cardUser = null;
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            cardUser = session.load(CardUser.class, cardUserId);
        } catch (Exception ex) {
            new ErrorPane().displayError(ex.getLocalizedMessage(), "Exception");
        }
        return cardUser;
    }
}
