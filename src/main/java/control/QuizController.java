package control;

import entity.Card;
import entity.CardUser;
import entity.Category;
import entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class QuizController {
    private final CategoryController categoryController = new CategoryController();

    public List<String> getCategoriesNames() {
        return categoryController.getAllCategories().stream().map(Category::getCategoryName).collect(Collectors.toList());
    }

    public Set<CardUser> getUserCards(User user) {
        user = new UserController().getUserByEmail(user.getEmail());
        return user.getCardUsers();
    }

    public List<Card> getCardsForQuiz(Set<CardUser> cardUsers, String selectedCategory) {
        List<Card> quizList = new ArrayList<>();
        for (CardUser cardUser : cardUsers) {
            Card card = cardUser.getCard();
            Set<Category> categoryList = card.getCategories();
            for (Category category : categoryList) {
                if (selectedCategory.equals(category.getCategoryName())) {
                    quizList.add(card);
                    break;
                }
            }
        }
        return quizList;
    }
}
