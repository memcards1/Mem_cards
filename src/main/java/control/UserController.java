package control;

import connection.HibernateConnection;
import entity.User;
import org.hibernate.Session;
import view.ErrorPane;

import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.List;

public class UserController {

    public void addUser(User user) {
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        } catch (PersistenceException e) {
            new ErrorPane().displayError("Пользователь с таким email уже существует",
                    "Ошибка регистрации");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User user) {
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            new ErrorPane().displayError("Произошла ошибка обновления данных пользователя",
                    "Ошибка обновления");
        }
    }

    public void deleteUser(User user) {
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            new ErrorPane().displayError("Произошла ошибка удаления пользователя",
                    "Ошибка удаления");
        }
    }

    public User getUserByEmail(String email) {
        User user = null;
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            user = session.load(User.class, email);
        } catch (Exception e) {
            new ErrorPane().displayError(e.getLocalizedMessage(),
                    "Ошибка");
        }
        return user;
    }

    public List<User> getAllUsers() {
        try (Session session = HibernateConnection.getSessionFactory().openSession()) {
            CriteriaQuery<User> criteria = session.getCriteriaBuilder().createQuery(User.class);
            criteria.from(User.class);
            return session.createQuery(criteria).getResultList();
        } catch (Exception ex) {
            new ErrorPane().displayError(ex.getLocalizedMessage(), "Exception");
            return new ArrayList<>();
        }
    }

}
