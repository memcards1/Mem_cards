package entity;

import javax.persistence.*;

@Entity
@Table(name = "card_user")
@AssociationOverrides({
        @AssociationOverride(name = "pk.card",
                joinColumns = @JoinColumn(name = "card_id")),
        @AssociationOverride(name = "pk.user",
                joinColumns = @JoinColumn(name = "user_email"))
})
public class CardUser {
    private CardUserId pk = new CardUserId();

    private int encountered = 0;
    private int correctAnswered = 0;

    private Privilege privilege;

    public CardUser() {
        // default constructor for Hibernate
    }

    @EmbeddedId
    public CardUserId getPk() {
        return pk;
    }

    public void setPk(CardUserId cardUserId) {
        this.pk = cardUserId;
    }

    @Transient
    public Card getCard() {
        return getPk().getCard();
    }

    public void setCard(Card card) {
        getPk().setCard(card);
    }

    @Transient
    public User getUser() {
        return getPk().getUser();
    }

    public void setUser(User user) {
        getPk().setUser(user);
    }

    @Column(name = "encounters")
    public int getEncountered() {
        return encountered;
    }

    public void setEncountered(int encountered) {
        this.encountered = encountered;
    }

    public void incrEncounters() {
        encountered++;
    }

    @Column(name = "cor_ans")
    public int getCorrectAnswered() {
        return correctAnswered;
    }

    public void setCorrectAnswered(int correctAnswered) {
        this.correctAnswered = correctAnswered;
    }

    public void incrCorrectAnswered() {
        correctAnswered++;
    }

    @Column(name = "privilege")
    public Privilege getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privilege privilege) {
        this.privilege = privilege;
    }

}
