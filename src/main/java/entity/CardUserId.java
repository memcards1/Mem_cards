package entity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CardUserId implements Serializable {
    private transient Card card;
    private transient User user;

    @ManyToOne
    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @ManyToOne
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardUserId that = (CardUserId) o;
        return Objects.equals(card, that.card) && Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(card, user);
    }
}
