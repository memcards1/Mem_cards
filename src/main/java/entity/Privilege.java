package entity;

public enum Privilege {
    REDACTOR, READER
}
