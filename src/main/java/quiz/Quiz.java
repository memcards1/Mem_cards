package quiz;

public class Quiz {
    public boolean checkCorrectAnswer(String answer, String correctAnswer) {
        answer = answer.replaceAll("\\s+", "").toUpperCase();
        correctAnswer = correctAnswer.replaceAll("\\s+", "").toUpperCase();
        return answer.equals(correctAnswer);
    }
}
