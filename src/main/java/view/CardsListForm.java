package view;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import control.CardController;
import control.CardUserController;
import control.UserController;
import entity.*;
import ui.ButtonStyle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Set;

public class CardsListForm extends JPanel {
    private JPanel mainPanel;
    private JList<Card> cardsList;
    private JButton newCardButton;
    private JPanel listPanel;
    private JFrame newCardForm = null;
    private transient User user;
    private JPopupMenu popupMenu;

    public CardsListForm(User user) {
        this.user = user;
        $$$setupUI$$$();
        initButtons();
        setButtonStyle();
        initListRenderer();
        initListListener();
        this.add(mainPanel);
        this.setVisible(true);
    }

    private void createUIComponents() {
        initList();
    }

    private void initList() {
        DefaultListModel<Card> listModel = new DefaultListModel<>();
        Set<CardUser> cardUsers = user.getCardUsers();
        cardUsers.stream().map(CardUser::getCard).forEach(listModel::addElement);
        cardsList = new JList<>(listModel);
    }

    public void updateList() {
        UserController userController = new UserController();
        user = userController.getUserByEmail(user.getEmail());
        ((DefaultListModel<Card>) cardsList.getModel()).removeAllElements();
        Set<CardUser> cardUsers = user.getCardUsers();
        cardUsers.stream().map(CardUser::getCard).forEach(((DefaultListModel<Card>) cardsList.getModel())::addElement);
    }

    private JPopupMenu initPopupMenu(Privilege privilege) {
        JPopupMenu popup = new JPopupMenu();

        JMenuItem shareItem = new JMenuItem("Поделиться");
        shareItem.addActionListener(e -> {
            AccessRightsForm accessRightsForm = new AccessRightsForm(cardsList.getSelectedValue());
            accessRightsForm.setVisible(true);
        });
        popup.add(shareItem);

        if (privilege == Privilege.REDACTOR) {
            JMenuItem editItem = new JMenuItem("Редактировать");
            editItem.addActionListener(e ->
                    new EditForm(cardsList.getSelectedValue(), new CardController(), this));
            popup.add(editItem);
        }

        JMenuItem removeItem = new JMenuItem("Удалить");
        removeItem.addActionListener(e -> {
            int result = JOptionPane.showConfirmDialog(mainPanel,
                    "Вы уверены, что хотите удалить карточку?");
            if (result == JOptionPane.YES_OPTION) {
                CardUserId cardUserId = new CardUserId();
                cardUserId.setUser(user);
                cardUserId.setCard(cardsList.getSelectedValue());
                CardUser cardUser = new CardUser();
                cardUser.setPk(cardUserId);
                new CardUserController().delete(cardUser);
                this.updateList();
            }
        });
        popup.add(removeItem);

        return popup;
    }

    private void initListListener() {
        cardsList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JList<Card> list = (JList<Card>) e.getSource();
                if (e.getClickCount() == 2) {
                    new CardForm(list.getSelectedValue(), user);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                check(e);
            }

            public void check(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    int preferredHeight = cardsList.getPreferredSize().height;
                    int mouseHeight = e.getPoint().y;
                    if (mouseHeight > preferredHeight)
                        return;
                    int row = cardsList.locationToIndex(e.getPoint());
                    cardsList.setSelectedIndex(row);
                    CardUserId cardUserId = new CardUserId();
                    cardUserId.setCard(cardsList.getSelectedValue());
                    cardUserId.setUser(user);
                    popupMenu = initPopupMenu(new CardUserController().getById(cardUserId).getPrivilege());
                    popupMenu.show(cardsList, e.getX(), e.getY());
                }
            }
        });
    }

    private void initListRenderer() {
        cardsList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                this.setHorizontalAlignment(SwingConstants.CENTER);
                ((JLabel) renderer).setText(((Card) value).getQuestion());
                return renderer;
            }
        });
    }

    private void initButtons() {
        newCardButton.addActionListener(e -> {
            newCardForm = new NewCardForm(user, this);
            FrameLocation.setFrameLocation(newCardForm);
            newCardForm.setVisible(true);
        });
    }

    private void setButtonStyle() {
        ButtonStyle buttonStyle = new ButtonStyle();
        newCardButton.setUI(buttonStyle);
        newCardButton.setBackground(new Color(0xF7A962E0, true));
        newCardButton.setForeground(Color.white);
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.setMinimumSize(new Dimension(500, 490));
        newCardButton = new JButton();
        newCardButton.setHorizontalAlignment(0);
        newCardButton.setText("Новая карточка");
        mainPanel.add(newCardButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        listPanel = new JPanel();
        listPanel.setLayout(new BorderLayout(0, 0));
        mainPanel.add(listPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(500, 400), null, 0, false));
        cardsList.setAlignmentX(0.0f);
        cardsList.setAlignmentY(0.0f);
        cardsList.setAutoscrolls(true);
        cardsList.setBackground(new Color(-1118482));
        cardsList.setFixedCellHeight(25);
        Font cardsListFont = this.$$$getFont$$$("Arial", Font.BOLD, 22, cardsList.getFont());
        if (cardsListFont != null) cardsList.setFont(cardsListFont);
        cardsList.setForeground(new Color(-16316665));
        cardsList.setLayoutOrientation(0);
        cardsList.setMinimumSize(new Dimension(500, 400));
        cardsList.setSelectionBackground(new Color(-1516824));
        cardsList.setSelectionForeground(new Color(-16777216));
        cardsList.setSelectionMode(0);
        listPanel.add(cardsList, BorderLayout.CENTER);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
