import control.*;
import entity.*;
import org.junit.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class QuizControllerTest {

    @Test
    public void testCategoriesNames() {

        CategoryController categoryController = new CategoryController();
        Category category1 = new Category();
        category1.setCategoryName("test_Category_1");
        categoryController.addCategory(category1);
        Category category2 = new Category();
        category2.setCategoryName("test_Category_2");
        categoryController.addCategory(category2);

        QuizController quizController = new QuizController();
        List<String> categoriesList = quizController.getCategoriesNames();
        List<String> expectedList = new ArrayList<>();
        expectedList.add("test_Category_1");
        expectedList.add("test_Category_2");
        Collections.sort(expectedList);
        Collections.sort(categoriesList);
        assertEquals(expectedList, categoriesList);
    }

    @Test
    public void testGetUserCards() {
        QuizController quizController = new QuizController();
        UserController userController = new UserController();
        CardController cardController = new CardController();
        CategoryController categoryController = new CategoryController();
        List<String> expectedCardsUser = new ArrayList<>();
        Set<Category> categorySet = new HashSet<>();
        CardUserController cardUserController = new CardUserController();
        User user = new User();
        user.setEmail("test1@test.com");
        user.setFirstName("test1");
        user.setLastName("test1");
        user.setPassword("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08");
        userController.addUser(user);
        user = new User();
        user.setEmail("test2@test.com");
        user.setFirstName("test2");
        user.setLastName("test2");
        user.setPassword("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08");
        userController.addUser(user);

        Card card1 = new Card();
        card1.setQuestion("Вопрос 1?");
        card1.setAnswer("Ответ 1");
        cardController.addCard(card1);
        CardUser cardUser = new CardUser();
        cardUser.setCard(card1);
        cardUser.setUser(userController.getUserByEmail("test1@test.com"));
        cardUser.setPrivilege(Privilege.REDACTOR);
        cardUserController.save(cardUser);
        categorySet.add(categoryController.getCategoryByName("test_Category_1"));
        card1.addCategories(categorySet);
        expectedCardsUser.add("Вопрос 1?");

        Card card2 = new Card();
        card2.setQuestion("Вопрос 2?");
        card2.setAnswer("Ответ 2");
        cardController.addCard(card2);
        cardUser = new CardUser();
        cardUser.setCard(card2);
        cardUser.setUser(userController.getUserByEmail("test1@test.com"));
        cardUser.setPrivilege(Privilege.REDACTOR);
        cardUserController.save(cardUser);
        card2.addCategories(categorySet);
        expectedCardsUser.add("Вопрос 2?");

        Card card3 = new Card();
        card3.setQuestion("Вопрос 3?");
        card3.setAnswer("Ответ 3");
        cardController.addCard(card3);
        cardUser = new CardUser();
        cardUser.setCard(card3);
        cardUser.setUser(userController.getUserByEmail("test1@test.com"));
        cardUser.setPrivilege(Privilege.REDACTOR);
        cardUserController.save(cardUser);
        expectedCardsUser.add("Вопрос 3?");

        user = userController.getUserByEmail("test2@test.com");
        Set<CardUser> cardsUser = quizController.getUserCards(user);
        Set<CardUser> expectedEmptyCardsUser = new HashSet<>();
        assertEquals(expectedEmptyCardsUser, cardsUser);

        List<String> realCardsUser = new ArrayList<>();
        user = userController.getUserByEmail("test1@test.com");
        cardsUser = quizController.getUserCards(user);
        for (CardUser cu : cardsUser) {
            realCardsUser.add(cu.getCard().getQuestion());
        }
        Collections.sort(expectedCardsUser);
        Collections.sort(realCardsUser);
        assertEquals(expectedCardsUser, realCardsUser);
    }

    @Test
    public void testGetCardsForQuiz() {
        QuizController quizController = new QuizController();
        UserController userController = new UserController();
        List<String> expectedCardsUser = new ArrayList<>();
        User user = userController.getUserByEmail("test1@test.com");
        Set<CardUser> cardsUser = quizController.getUserCards(user);
        List<Card> realList = quizController.getCardsForQuiz(cardsUser, "test_Category_2");
        List<String> realCardsUser = new ArrayList<>();
        for (Card cu : realList) {
            realCardsUser.add(cu.getQuestion());
        }
        assertEquals(expectedCardsUser, realCardsUser);
    }

}