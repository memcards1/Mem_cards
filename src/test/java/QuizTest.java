import org.junit.jupiter.api.Test;
import quiz.Quiz;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QuizTest {
    @Test
    void checkCorrectAnswer() {
        Quiz quiz = new Quiz();
        String expectedAnswer = "This is the answer";
        String realAnswer = "This is the answer";
        assertTrue(quiz.checkCorrectAnswer(realAnswer, expectedAnswer));

        realAnswer = "THIS IS THE ANSWER";
        assertTrue(quiz.checkCorrectAnswer(realAnswer, expectedAnswer));

        realAnswer = "this  is  the  answer";
        assertTrue(quiz.checkCorrectAnswer(realAnswer, expectedAnswer));

        realAnswer = "thisistheanswer";
        assertTrue(quiz.checkCorrectAnswer(realAnswer, expectedAnswer));
    }

    @Test
    void checkIncorrectAnswer() {
        Quiz quiz = new Quiz();
        String expectedAnswer = "This is the answer";
        String realAnswer = "and this is the answer";
        assertFalse(quiz.checkCorrectAnswer(realAnswer, expectedAnswer));
        realAnswer = "this is the answer.";
        assertFalse(quiz.checkCorrectAnswer(realAnswer, expectedAnswer));
        realAnswer = "this is the answers";
        assertFalse(quiz.checkCorrectAnswer(realAnswer, expectedAnswer));
    }
}
